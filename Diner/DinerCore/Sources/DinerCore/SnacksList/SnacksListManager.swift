//
//  SnacksListManager.swift
//  Diner
//
//  Copyright © 2020 nissi.miranda. All rights reserved.
//

import Foundation

public protocol SnacksListManager {
    typealias RemoteDataHandler = () -> Void
    
    func getMenuSnacks(_ completion: @escaping RemoteDataHandler)
    func getIngredients(_ completion: @escaping RemoteDataHandler)
    
    func getMenuSnackOfType(_ type: SnackType, withAmount amount: Double) -> Snack?
    func getCustomSnackWithIngredients(_ ingredients: [Ingredient], andAmount amount: Double) -> Snack
    func getIngredientOfType(_ type: IngredientType, withAmount amount: Double) -> Ingredient?
    
    func getDiscountPriceForOrder(_ order: [Snack]) -> Double
    func getDiscountPriceForSnack(_ snack: Snack) -> Double
}

public final class SnacksListManagerImplemenation: SnacksListManager {
    
    private let ingredientGateway: IngredientGateway
    private let snackGateway: SnackGateway
    
    public init(ingredientGateway: IngredientGateway, snackGateway: SnackGateway) {
        self.ingredientGateway = ingredientGateway
        self.snackGateway = snackGateway
    }
    
    var remoteSnacks: [RemoteSnack] = []
    var remoteIngredients: [RemoteIngredient] = []
    
    public func getMenuSnacks(_ completion: @escaping RemoteDataHandler) {
        
        snackGateway.getRemoteSnacks({
            (remoteSnacks, errorMessage) in
            if let remoteSnacks = remoteSnacks {
                self.remoteSnacks = remoteSnacks
            }
            completion()
        })
    }
    
    public func getIngredients(_ completion: @escaping RemoteDataHandler) {
        
        ingredientGateway.getRemoteIngredients({
            (remoteIngredients, errorMessage) in
            if let remoteIngredients = remoteIngredients {
                self.remoteIngredients = remoteIngredients
            }
            completion()
        })
    }
    
    private func getSales() -> [Sale] {
        return [LightSale(), MuchMeetSale(), MuchCheeseSale()]
    }
}

// MARK: - Snacks & Ingredients creation

extension SnacksListManagerImplemenation {
    
    public func getMenuSnackOfType(_ type: SnackType, withAmount amount: Double) -> Snack? {
        
        guard let snacksInfo = remoteSnacks.filter( {$0.name == type.rawValue }).first else {
            return nil
        }
        
        var ingredients = [Ingredient]()
        
        for ingredientName in snacksInfo.ingredients {
            let ingredientType = IngredientType.byName(name: ingredientName)
            if let ingredient = getIngredientOfType(ingredientType, withAmount: 1) {
                ingredients.append(ingredient)
            }
        }
        
        guard ingredients.count == snacksInfo.ingredients.count else {
            return nil
        }
        
        return Snack(name: type, ingredients: ingredients, amount: amount)
    }
    
    public func getCustomSnackWithIngredients(_ ingredients: [Ingredient], andAmount amount: Double) -> Snack {
        return Snack(name: .custom, ingredients: ingredients, amount: amount)
    }
    
    public func getIngredientOfType(_ type: IngredientType, withAmount amount: Double) -> Ingredient? {
        
        guard let ingredientInfo = remoteIngredients.filter( {$0.name == type.rawValue }).first else {
            return nil
        }
        return Ingredient(name: type, price: ingredientInfo.price, amount: amount)
    }
}

// MARK: - Prices calculation

extension SnacksListManagerImplemenation {
    
    public func getDiscountPriceForOrder(_ order: [Snack]) -> Double {
        
        let sales = getSales()
        
        guard sales.count > 0 else {
            return getFullPriceForOrder(order)
        }
        
        var orderPrice: Double = 0.0
        for snack in order {
            orderPrice += getDiscountPriceForSnack(snack)
        }
        return orderPrice
    }
    
    private func getFullPriceForOrder(_ order: [Snack]) -> Double {
        
        var orderPrice: Double = 0.0
        for snack in order {
            orderPrice += getFullPriceForSnack(snack)
        }
        return orderPrice
    }
    
    // MARK: - Snack prices
    
    private func getFullPriceForSnack(_ snack: Snack) -> Double {
        
        var snackPrice: Double = 0.0
        for ingredient in snack.ingredients {
            snackPrice += getFullPriceForIngredient(ingredient)
        }
        return snackPrice * snack.amount
    }
    
    public func getDiscountPriceForSnack(_ snack: Snack) -> Double {
        
        var prices = [Double]()
        
        for sale in getSales() {
            if let salePrice = sale.getSalePrice(snack) {
                prices.append(salePrice)
            }
        }
        
        if prices.count > 0 {
            return prices.min() ?? 0
        }
        return getFullPriceForSnack(snack)
    }
    
    // MARK: - Ingredient prices
    
    private func getFullPriceForIngredient(_ ingredient: Ingredient) -> Double {
        return ingredient.price * ingredient.amount
    }
}
