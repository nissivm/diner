//
//  SnackGateway.swift
//  Diner
//
//  Copyright © 2020 nissi.miranda. All rights reserved.
//

import Foundation

public protocol SnackGateway {
    typealias GetRemoteSnacksHandler = (_ remoteSnacks: [RemoteSnack]?, _ errorMessage: String?) -> Void
    func getRemoteSnacks(_ completion: @escaping GetRemoteSnacksHandler)
}
