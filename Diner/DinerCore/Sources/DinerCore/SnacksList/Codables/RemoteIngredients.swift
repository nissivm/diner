//
//  RemoteIngredients.swift
//  Diner
//
//  Copyright © 2020 nissi.miranda. All rights reserved.
//

import Foundation

public struct RemoteIngredients: Codable {
    let ingredients: [RemoteIngredient]
}

public struct RemoteIngredient: Codable {
    let name: String
    let price: Double
}
