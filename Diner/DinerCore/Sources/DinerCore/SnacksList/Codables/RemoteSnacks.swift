//
//  RemoteSnacks.swift
//  Diner
//
//  Copyright © 2020 nissi.miranda. All rights reserved.
//

import Foundation

public struct RemoteSnacks: Codable {
    let snacks: [RemoteSnack]
}

public struct RemoteSnack: Codable {
    let name: String
    let ingredients: [String]
}
