//
//  IngredientGateway.swift
//  Diner
//
//  Copyright © 2020 nissi.miranda. All rights reserved.
//

import Foundation

public protocol IngredientGateway {
    typealias GetRemoteIngredientsHandler = (_ remoteIngredients: [RemoteIngredient]?, _ errorMessage: String?) -> Void
    func getRemoteIngredients(_ completion: @escaping GetRemoteIngredientsHandler)
}
