//
//  SnacksListService.swift
//  Diner
//
//  Copyright © 2020 nissi.miranda. All rights reserved.
//

import Foundation

public protocol SnacksListService {
    typealias RemoteDataHandler = () -> Void
    
    func getSnackOfType(_ type: SnackType, amount a: Double, andIngredients ingredients: [Ingredient]) -> Snack?
    func getIngredientOfType(_ type: IngredientType, withAmount amount: Double) -> Ingredient?
    func getPriceForOrder(_ order: [Snack]) -> Double
    func loadDataDependencies(_ completion: @escaping RemoteDataHandler)
}

public final class SnacksListServiceImplementation: SnacksListService {
    
    let snacksListManager: SnacksListManager
    
    public init(snacksListManager: SnacksListManager) {
        self.snacksListManager = snacksListManager
    }
    
    public func getSnackOfType(_ type: SnackType, amount a: Double, andIngredients ingredients: [Ingredient]) -> Snack? {
        
        if type == .custom {
            return snacksListManager.getCustomSnackWithIngredients(ingredients, andAmount: a)
        }
        else {
            return snacksListManager.getMenuSnackOfType(type, withAmount: a)
        }
    }
    
    public func getIngredientOfType(_ type: IngredientType, withAmount amount: Double) -> Ingredient? {
        snacksListManager.getIngredientOfType(type, withAmount: amount)
    }
    
    public func getPriceForOrder(_ order: [Snack]) -> Double {
        return snacksListManager.getDiscountPriceForOrder(order)
    }
    
    public func loadDataDependencies(_ completion: @escaping RemoteDataHandler) {
        let group = DispatchGroup()
        
        group.enter()
        snacksListManager.getIngredients({() in
            group.leave()
        })
        
        group.enter()
        snacksListManager.getMenuSnacks({() in
            group.leave()
        })
        
        group.notify(queue: .main, execute: {() in
            completion()
        })
    }
}
