//
//  Ingredient.swift
//  Diner
//
//  Copyright © 2020 nissi.miranda. All rights reserved.
//

import Foundation

public enum IngredientType: String, CaseIterable {
    case lettuce = "lettuce"
    case bacon = "bacon"
    case meet = "meet"
    case egg = "egg"
    case cheese = "cheese"
    case none = "none"
    
    static func byName(name: String) -> IngredientType {
        return IngredientType.allCases.first(where: { $0.rawValue == name }) ?? .none
    }
}

public struct Ingredient {
    let name: IngredientType
    let price: Double
    let amount: Double
}
