//
//  Snack.swift
//  Diner
//
//  Copyright © 2020 nissi.miranda. All rights reserved.
//

import Foundation

public enum SnackType: String {
    case xBacon = "xBacon"
    case xBurguer = "xBurguer"
    case xEgg = "xEgg"
    case xEggBacon = "xEggBacon"
    case custom = "custom"
}

public struct Snack {
    let name: SnackType
    let ingredients: [Ingredient]
    let amount: Double
}
