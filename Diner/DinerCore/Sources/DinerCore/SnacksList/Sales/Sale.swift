//
//  Sale.swift
//  Diner
//
//  Copyright © 2020 nissi.miranda. All rights reserved.
//

import Foundation

public protocol Sale {
    func getSalePrice(_ snack: Snack) -> Double?
}
