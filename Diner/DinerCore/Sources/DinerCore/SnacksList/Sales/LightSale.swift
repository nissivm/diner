//
//  LightSale.swift
//  Diner
//
//  Copyright © 2020 nissi.miranda. All rights reserved.
//

import Foundation

public struct LightSale: Sale {
    
    public func getSalePrice(_ snack: Snack) -> Double? {
        
        let lettuce = snack.ingredients.filter { $0.name == IngredientType.lettuce }
        let bacon = snack.ingredients.filter { $0.name == IngredientType.bacon }
        
        guard (lettuce.count >= 1) && (bacon.count == 0) else {
            return nil
        }
        
        var totalPrice: Double = 0.0
        for ingredient in snack.ingredients {
            totalPrice += (ingredient.price * ingredient.amount)
        }
        
        return (totalPrice * 0.9) * snack.amount
    }
}
