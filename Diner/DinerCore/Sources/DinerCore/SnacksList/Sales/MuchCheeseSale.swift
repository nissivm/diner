//
//  MuchCheeseSale.swift
//  Diner
//
//  Copyright © 2020 nissi.miranda. All rights reserved.
//

import Foundation

public struct MuchCheeseSale: Sale {
    
    public func getSalePrice(_ snack: Snack) -> Double? {
        
        let cheeseAmount = snack.ingredients.filter { $0.name == IngredientType.cheese }.first?.amount ?? 0
        
        guard cheeseAmount >= 3 else {
            return nil
        }
        
        let cheeseAmountToPay = (cheeseAmount * 2)/3
        var cheesePrice = 0.0
        var remainedPrice = 0.0
        
        for ingredient in snack.ingredients {
            if ingredient.name == IngredientType.cheese {
                cheesePrice = cheeseAmountToPay * ingredient.price
            }
            else {
                remainedPrice += (ingredient.price * ingredient.amount)
            }
        }
        
        return (cheesePrice + remainedPrice) * snack.amount
    }
}
