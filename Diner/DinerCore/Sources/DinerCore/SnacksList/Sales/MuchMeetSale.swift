//
//  MuchMeetSale.swift
//  Diner
//
//  Copyright © 2020 nissi.miranda. All rights reserved.
//

import Foundation

public struct MuchMeetSale: Sale {
    
    public func getSalePrice(_ snack: Snack) -> Double? {
        
        let meetAmount = snack.ingredients.filter { $0.name == IngredientType.meet }.first?.amount ?? 0
        
        guard meetAmount >= 3 else {
            return nil
        }
        
        let meetAmountToPay = (meetAmount * 2)/3
        var meetPrice = 0.0
        var remainedPrice = 0.0
        
        for ingredient in snack.ingredients {
            if ingredient.name == IngredientType.meet {
                meetPrice = meetAmountToPay * ingredient.price
            }
            else {
                remainedPrice += (ingredient.price * ingredient.amount)
            }
        }
        
        return (meetPrice + remainedPrice) * snack.amount
    }
}
