import XCTest

import DinerCoreTests

var tests = [XCTestCaseEntry]()
tests += DinerCoreTests.allTests()
XCTMain(tests)
