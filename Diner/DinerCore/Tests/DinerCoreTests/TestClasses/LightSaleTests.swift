//
//  LightSaleTests.swift
//  DinerTests
//
//  Created by Nissi Vieira Miranda on 25/01/2020.
//  Copyright © 2020 nissi.miranda. All rights reserved.
//

import XCTest
import Foundation
@testable import DinerCore

class LightSaleTests: XCTestCase {

    func test_snack_price_gets_correct_discount_when_eligible_to_light_sale() {
        
        // Given
        var ingredients = [Ingredient]()
        ingredients.append(Ingredient(name: .lettuce, price: 0.4, amount: 2))
        ingredients.append(Ingredient(name: .meet, price: 3, amount: 1))
        ingredients.append(Ingredient(name: .cheese, price: 3.5, amount: 2))
        let snack = Snack(name: .custom, ingredients: ingredients, amount: 3)
        
        // When
        let fullPrice = 32.40
        let priceExpected = fullPrice * 0.9
        let discountPrice = LightSale().getSalePrice(snack) ?? 0
        
        // Should
        XCTAssertEqual(priceExpected, discountPrice, accuracy: 0.01)
    }
}
