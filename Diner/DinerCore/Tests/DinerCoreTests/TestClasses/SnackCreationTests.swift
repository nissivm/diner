//
//  SnackCreationTests.swift
//  DinerTests
//
//  Created by nissi.miranda on 1/31/20.
//  Copyright © 2020 nissi.miranda. All rights reserved.
//

import XCTest
import Foundation
@testable import DinerCore

class SnackCreationTests: XCTestCase {
    
    var sut: SnacksListManagerImplemenation!
    
    override func setUp() {
        let ingredientGateway = MockIngredientGatewayImplementation()
        let snackGateway = MockSnackGatewayImplementation()
        sut = SnacksListManagerImplemenation(ingredientGateway: ingredientGateway, snackGateway: snackGateway)
        sut.remoteIngredients = [RemoteIngredient(name: "lettuce", price: 0.4),
        RemoteIngredient(name: "bacon", price: 2.0),
        RemoteIngredient(name: "meet", price: 3.0),
        RemoteIngredient(name: "egg", price: 0.8),
        RemoteIngredient(name: "cheese", price: 1.5)]
        sut.remoteSnacks = [RemoteSnack(name: "xBacon", ingredients: ["bacon", "meet", "cheese"]),
        RemoteSnack(name: "xBurguer", ingredients: ["meet", "cheese", "ham"]),
        RemoteSnack(name: "xEgg", ingredients: ["egg", "meet", "cheese"]),
        RemoteSnack(name: "xEggBacon", ingredients: ["egg", "bacon", "meet", "cheese"])]
    }

    override func tearDown() {
        sut.remoteIngredients.removeAll()
        sut.remoteSnacks.removeAll()
        sut = nil
    }

    func test_snack_is_created_if_remote_ingredients_contains_all_snack_ingredients() {
        
        // Given
        let snackType: SnackType = .xBacon
        
        // When
        let snack = sut.getMenuSnackOfType(snackType, withAmount: 1)
        
        // Should
        XCTAssertNotNil(snack)
    }
    
    func test_snack_is_not_created_if_remote_ingredients_contains_not_all_snack_ingredients() {
        
        // Given
        let snackType: SnackType = .xBurguer
        
        // When
        let snack = sut.getMenuSnackOfType(snackType, withAmount: 1)
        
        // Should
        XCTAssertNil(snack)
    }
}
