//
//  LoadDataDependenciesTests.swift
//  DinerTests
//
//  Created by nissi.miranda on 1/30/20.
//  Copyright © 2020 nissi.miranda. All rights reserved.
//

import XCTest
import Foundation
@testable import DinerCore

class LoadDataDependenciesTests: XCTestCase {
    
    var sut: SnacksListManagerImplemenation!
    
    override func setUp() {
        let ingredientGateway = MockIngredientGatewayImplementation()
        let snackGateway = MockSnackGatewayImplementation()
        sut = SnacksListManagerImplemenation(ingredientGateway: ingredientGateway, snackGateway: snackGateway)
    }
    
    override func tearDown() {
        sut = nil
    }
    
    func test_remote_ingredients_array_gets_filled_when_receiving_successfull_response_from_gateway() {
        
        let expectationResult = expectation(description: "result expectation")
        
        // Given
        XCTAssertTrue(sut.remoteIngredients.count == 0)
        
        // When
        sut.getIngredients({() in
            expectationResult.fulfill()
            
            // Should
            XCTAssertTrue(self.sut.remoteIngredients.count > 0, "Remote ingredients array not fullfilled")
        })
        
        waitForExpectations(timeout: 1)
    }
    
    func test_remote_snacks_array_gets_filled_when_receiving_successfull_response_from_gateway() {
        
        let expectationResult = expectation(description: "result expectation")
        
        // Given
        XCTAssertTrue(sut.remoteSnacks.count == 0)
        
        // When
        sut.getMenuSnacks({() in
            expectationResult.fulfill()
            
            // Should
            XCTAssertTrue(self.sut.remoteSnacks.count > 0, "Remote snacks array not fullfilled")
        })
        
        waitForExpectations(timeout: 1)
    }
}
