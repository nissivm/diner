//
//  NoSaleTests.swift
//  DinerTests
//
//  Created by nissi.miranda on 1/30/20.
//  Copyright © 2020 nissi.miranda. All rights reserved.
//

import XCTest
import Foundation
@testable import DinerCore

class NoSaleTests: XCTestCase {
    
    var sut: SnacksListManagerImplemenation!
    
    override func setUp() {
        let ingredientGateway = MockIngredientGatewayImplementation()
        let snackGateway = MockSnackGatewayImplementation()
        sut = SnacksListManagerImplemenation(ingredientGateway: ingredientGateway, snackGateway: snackGateway)
    }
    
    override func tearDown() {
        sut = nil
    }

    func test_snack_full_price_is_returned_if_not_eligible_to_any_sales() {
        
        // Given
        var ingredients = [Ingredient]()
        ingredients.append(Ingredient(name: .lettuce, price: 0.4, amount: 2))
        ingredients.append(Ingredient(name: .bacon, price: 2, amount: 3))
        ingredients.append(Ingredient(name: .cheese, price: 1.5, amount: 2))
        ingredients.append(Ingredient(name: .egg, price: 0.8, amount: 1))
        let snack = Snack(name: .custom, ingredients: ingredients, amount: 4)
        
        // When
        let priceExpected = 42.4
        let snackPrice = sut.getDiscountPriceForSnack(snack)
        
        // Should
        XCTAssertEqual(priceExpected, snackPrice, accuracy: 0.01)
    }
}
