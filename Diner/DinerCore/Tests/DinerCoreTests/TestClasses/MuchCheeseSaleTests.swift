//
//  MuchCheeseSaleTests.swift
//  DinerTests
//
//  Created by Nissi Vieira Miranda on 26/01/2020.
//  Copyright © 2020 nissi.miranda. All rights reserved.
//

import XCTest
import Foundation
@testable import DinerCore

class MuchCheeseSaleTests: XCTestCase {

    func test_snack_price_gets_correct_discount_when_eligible_to_much_cheese_sale() {
        
        // Given
        var ingredients = [Ingredient]()
        ingredients.append(Ingredient(name: .bacon, price: 2, amount: 1))
        ingredients.append(Ingredient(name: .cheese, price: 1.5, amount: 8))
        ingredients.append(Ingredient(name: .egg, price: 0.8, amount: 1))
        let snack = Snack(name: .custom, ingredients: ingredients, amount: 4)
        
        // When
        let priceExpected = 43.2
        let discountPrice = MuchCheeseSale().getSalePrice(snack) ?? 0
        
        // Should
        XCTAssertEqual(priceExpected, discountPrice, accuracy: 0.01)
    }
}
