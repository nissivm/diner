//
//  MockIngredientGatewayImplementation.swift
//  DinerTests
//
//  Created by nissi.miranda on 1/30/20.
//  Copyright © 2020 nissi.miranda. All rights reserved.
//

import Foundation
@testable import DinerCore

final class MockIngredientGatewayImplementation: IngredientGateway {
    func getRemoteIngredients(_ completion: @escaping GetRemoteIngredientsHandler) {
        let remoteIngredients = [RemoteIngredient(name: "Ingredient1", price: 1.0),
        RemoteIngredient(name: "Ingredient2", price: 2.0),
        RemoteIngredient(name: "Ingredient3", price: 3.0)]
        completion(remoteIngredients, nil)
    }
}
