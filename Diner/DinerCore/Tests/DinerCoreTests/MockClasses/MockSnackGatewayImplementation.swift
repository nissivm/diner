//
//  MockSnackGatewayImplementation.swift
//  DinerTests
//
//  Created by nissi.miranda on 1/30/20.
//  Copyright © 2020 nissi.miranda. All rights reserved.
//

import Foundation
@testable import DinerCore

final class MockSnackGatewayImplementation: SnackGateway {
    func getRemoteSnacks(_ completion: @escaping GetRemoteSnacksHandler) {
        let remoteSnacks = [RemoteSnack(name: "Snack1", ingredients: ["Ingredient1", "Ingredient2"]),
        RemoteSnack(name: "Snack2", ingredients: ["Ingredient1", "Ingredient2"]),
        RemoteSnack(name: "Snack3", ingredients: ["Ingredient1", "Ingredient2"])]
        completion(remoteSnacks, nil)
    }
}
