//
//  SnacksListPresenter.swift
//  Diner
//
//  Created by nissi.miranda on 1/24/20.
//  Copyright © 2020 nissi.miranda. All rights reserved.
//

import Foundation
import DinerCore

public protocol SnacksListView: class {
    var presenter: SnacksListPresenter! { get set }
    func loading(_ show: Bool)
    func showSnacksInfo(_ info: [String: Double])
    func displayOrderTotal(_ total: Double)
}

public protocol SnacksListPresenter {
    func viewDidLoad()
    func pickedMenuSnackWithName(_ name: SnackType, andAmount amount: Double)
    func removedMenuSnackWithName(_ name: SnackType, andAmount amount: Double)
    func createOrderButtonPressed()
}

public final class SnacksListPresenterImplementation: SnacksListPresenter {
    
    var order = [Snack]()
    
    private let router: SnacksListRouter
    private weak var view: SnacksListView!
    private let snacksListService: SnacksListService
    
    public init(router: SnacksListRouter, view: SnacksListView, snacksListService: SnacksListService) {
        self.router = router
        self.view = view
        self.snacksListService = snacksListService
    }
    
    public func viewDidLoad() {
        view.loading(true)
        snacksListService.loadDataDependencies({() in
            self.finishedLoadingDataDependencies()
        })
    }
    
    public func pickedMenuSnackWithName(_ name: SnackType, andAmount amount: Double) {
    }
    
    public func removedMenuSnackWithName(_ name: SnackType, andAmount amount: Double) {
    }
    
    public func createOrderButtonPressed() {
    }
    
    private func finishedLoadingDataDependencies() {
        view.loading(false)
        view.showSnacksInfo(createSnacksInfoDictionary())
    }
    
    private func createSnacksInfoDictionary() -> [String: Double] {
        // TODO: Use remoteSnacks and remoteIngredients arrays to create dictionary
        return [String: Double]()
    }
}
