import XCTest

import DinerPresentationTests

var tests = [XCTestCaseEntry]()
tests += DinerPresentationTests.allTests()
XCTMain(tests)
