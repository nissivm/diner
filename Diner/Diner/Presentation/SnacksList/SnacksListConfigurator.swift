//
//  SnacksListConfigurator.swift
//  Diner
//
//  Created by nissi.miranda on 1/27/20.
//  Copyright © 2020 nissi.miranda. All rights reserved.
//

import Foundation
import DinerCore
import DinerPresentation

protocol SnacksListConfigurator {
    func configure(snacksListViewController: SnacksListView)
}

class SnacksListConfiguratorImplementation: SnacksListConfigurator {
    func configure(snacksListViewController: SnacksListView) {
        
        let router = SnacksListRouterImplementation(viewController: snacksListViewController)
        
        let ingredientGateway = IngredientGatewayImplementation()
        let snackGateway = SnackGatewayImplementation()
        let manager = SnacksListManagerImplemenation(ingredientGateway: ingredientGateway, snackGateway: snackGateway)
        let service = SnacksListServiceImplementation(snacksListManager: manager)
        
        let presenter = SnacksListPresenterImplementation(router: router, view: snacksListViewController, snacksListService: service)
        
        snacksListViewController.presenter = presenter
    }
}
