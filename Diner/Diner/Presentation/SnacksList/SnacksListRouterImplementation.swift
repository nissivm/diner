//
//  SnacksListRouterImplementation.swift
//  Diner
//
//  Created by nissi.miranda on 1/31/20.
//  Copyright © 2020 nissi.miranda. All rights reserved.
//

import Foundation
import DinerPresentation

final class SnacksListRouterImplementation: SnacksListRouter {
    
    private weak var viewController: SnacksListView!
    
    init(viewController: SnacksListView) {
        self.viewController = viewController
    }
}
