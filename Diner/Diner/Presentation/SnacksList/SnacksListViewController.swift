//
//  SnacksListViewController.swift
//  Diner
//
//  Created by nissi.miranda on 1/27/20.
//  Copyright © 2020 nissi.miranda. All rights reserved.
//

import UIKit
import DinerPresentation

class SnacksListViewController: UIViewController, SnacksListView {
    
    private var configurator = SnacksListConfiguratorImplementation()
    var presenter: SnacksListPresenter!

    override func viewDidLoad() {
        super.viewDidLoad()
        configurator.configure(snacksListViewController: self)
        presenter.viewDidLoad()
    }
    
    func loading(_ show: Bool) {
    }
    
    func showSnacksInfo(_ info: [String: Double]) {
    }
    
    func displayOrderTotal(_ total: Double) {
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
