//
//  SnackGatewayImplementation.swift
//  Diner
//
//  Created by nissi.miranda on 1/31/20.
//  Copyright © 2020 nissi.miranda. All rights reserved.
//

import Foundation
import DinerCore

final class SnackGatewayImplementation: SnackGateway {
    func getRemoteSnacks(_ completion: @escaping GetRemoteSnacksHandler) {
        // TODO: Call API OR get data from local data bank
        completion([RemoteSnack](), nil)
    }
}
