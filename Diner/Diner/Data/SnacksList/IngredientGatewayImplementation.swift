//
//  IngredientGatewayImplementation.swift
//  Diner
//
//  Created by nissi.miranda on 1/31/20.
//  Copyright © 2020 nissi.miranda. All rights reserved.
//

import Foundation
import DinerCore

final class IngredientGatewayImplementation: IngredientGateway {
    func getRemoteIngredients(_ completion: @escaping GetRemoteIngredientsHandler) {
        // TODO: Call API OR get data from local data bank
        completion([RemoteIngredient](), nil)
    }
}
