//
//  AppDelegate.swift
//  Diner
//
//  Created by nissi.miranda on 1/31/20.
//  Copyright © 2020 nissi.miranda. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        return true
    }
}

